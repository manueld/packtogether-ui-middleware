
const express = require('express');
const router = express.Router();
const R = require('ramda');

const { putToCache, removeFromCache, getCachedItemByKey, sections: cacheSections } = require('../dataCache');
const publicPaths = [
    '/api/auth',
    '/api/logout',
    '/api/register'
];
const expiredFilter = validDateTime => !validDateTime || new Date(validDateTime).getTime() < new Date().getTime();
const omitToken = R.omit(['authToken']);
const pickToken = R.pick(['authToken']);
const pickTokenProp = R.prop('authToken');

module.exports = client => {
    router.all('/api/*', (req, res, next) => {
        if (publicPaths.includes(req.url)) {
            next();
            return;
        }
        console.log(`Request url "${req.url}" is secured, check for token...`);
        let knownAndExpired = false;
        if (req.cookies && req.cookies.authToken) {
            const knownToken = getCachedItemByKey('authTokens', req.cookies.authToken);
            if (knownToken) {
                const expired = expiredFilter(knownToken.tokenValidUntil);
                if (!expired) {
                    next(); // Allow further processing (next middleware/router)
                    return;
                }
                knownAndExpired = true;
            }
        }
        console.log(`Rejected request due to ${(knownAndExpired ? 'expired' : 'unknown')} token "${req.cookies && req.cookies.authToken}"`);
        res.status(401).end();
    });

    router.post('/api/auth', async (req, res) => {
        try {

            const knownToken = getCachedItemByKey('authTokens', req.cookies && req.cookies.authToken);
            if (knownToken) {
                return res.json(pickToken(knownToken)).end();
            }
            console.log('Sending user authentication cote request...');
            client.send({
                type: 'authenticate',
                query: {
                    emailAddress: req.body.emailAddress,
                    password: req.body.password
                }
            }, (err, response) => {
                console.log('Response on /api/auth client', response);
                if (!response) {
                    return res.status(404).json({ errKey: 'authentication_error' });
                }
                if (response.found) {
                    const token = pickTokenProp(response.data);
                    putToCache('authTokens', token, omitToken(response.data));
                    res.cookie('authToken', token).status(201).end(); // Respond to UI only the token, never the user id!
                    return;
                }
                res.status(401).json({ errKey: 'authentication_credentials_mismatch' });
                return;
            });
        } catch (err) {
            console.error(err);
            res.status(500).send({ err });
        }
    });

    router.post('/api/logout', async (req, res) => {
        try {
            client.send({
                type: 'logout',
                query: {
                    authToken: req.cookies && req.cookies.authToken
                }
            }, (err, response) => {
                console.log('logout Response, (err, response)', { err, response });
                removeFromCache('authTokens', req.cookies.authToken);
                res.cookie('authToken', '', { expires: new Date('1970-01-01') });
                if (err) {
                    console.error(err);
                    res.status(500).send({ err });
                } else {
                    res.send({ success: true });
                }
            });
        } catch (err) {
            console.error(err);
            res.cookie('authToken', '', { expires: new Date('1970-01-01') }).status(500).send({ err });
        }
    });

    router.post('/api/register', async (req, res) => {
        try {
            console.log('Sending registration cote request...');
            client.send({
                type: 'register',
                query: {
                    firstName: req.body.firstName,
                    lastName: req.body.lastName,
                    emailAddress: req.body.emailAddress,
                    password: req.body.password
                }
            }, (err, response) => {
                console.log('Response on /api/register client', response);
                if (response && response.success) {
                    const token = pickTokenProp(response.data);
                    putToCache('authTokens', token, omitToken(response.data));
                    res.cookie('authToken', token).status(201).end(); // Respond to UI only the token, never the user id!
                    return;
                }
                return res.status(400).json({ errKey: response.errKey }).end();
            });
        } catch (err) {
            console.error(err);
            res.status(500).send({ err });
        }
    });

    return router;
};
