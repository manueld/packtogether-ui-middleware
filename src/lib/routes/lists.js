
const express = require('express');
const router = express.Router();

const { getCachedItemByKey } = require('../dataCache');

module.exports = client => {
    router.get('/api/lists', async (req, res) => {
        try {
            const cachedTokenEntry = getCachedItemByKey('authTokens', req.cookies && req.cookies.authToken);
            if (!cachedTokenEntry) {
                return res.status(401).end();
            }
            client.send({
                type: 'getAllListsByUserId',
                query: {
                    userId: cachedTokenEntry.userId
                }
            }, (err, response) => {
                if (err) {
                    console.error(err);
                    return res.status(500).json({ err }).end();
                }
                if (response.errCode) {
                    res.status(response.errCode).json({ errKey: response.errKey }).end();
                    return;
                }
                res.json({ data: response.data });
                return;
            });
        } catch(err) {
            console.error(err);
            res.status(500).send({ err });
        }
    });

    router.post('/api/lists', async (req, res) => {
        
        try {
            const cachedTokenEntry = getCachedItemByKey('authTokens', req.cookies && req.cookies.authToken);
            if (!cachedTokenEntry) {
                return res.status(401).end();
            }
            console.log('Cote request "addList" with query', {
                userId: cachedTokenEntry.userId,
                listTitle: req.body.listTitle
            });
            client.send({
                type: 'addList',
                query: {
                    userId: cachedTokenEntry.userId,
                    listTitle: req.body.listTitle
                }
            }, (err, response) => {
                if (err) {
                    console.error(err);
                    return res.status(500).json({ err }).end();
                }
                if (response.errCode) {
                    res.status(response.errCode).json({ errKey: response.errKey }).end();
                    return;
                }
                res.json({
                    data: response.data,
                    newListId:response.newListId
                });
                return;
            });
        } catch(err) {
            console.error(err);
            res.status(500).send({ err });
        }
    });

    router.put('/api/lists/:listId/items/:itemId/counter', async (req, res) => {
        try {
            const cachedTokenEntry = getCachedItemByKey('authTokens', req.cookies && req.cookies.authToken);
            if (!cachedTokenEntry) {
                return res.status(401).end();
            }
            client.send({
                type: 'setListitemCountPacked',
                query: {
                    ...req.params,
                    newValue: req.query.newValue,
                    userId: cachedTokenEntry.userId
                }
            }, (err, response) => {
                if (err) {
                    console.error(err);
                    return res.status(500).json({ err }).end();
                }
                if (response.errCode) {
                    res.status(response.errCode).json({ errKey: response.errKey }).end();
                    return;
                }
                res.json({
                    gotUpdate: response.data.gotUpdate
                });
                return;
            });
        } catch(err) {
            console.error(err);
            res.status(500).send({ err });
        }
    });

    router.post('/api/lists/:listId/groups/:groupId/items', async (req, res) => {
        try {
            const cachedTokenEntry = getCachedItemByKey('authTokens', req.cookies && req.cookies.authToken);
            if (!cachedTokenEntry) {
                return res.status(401).end();
            }
            client.send({
                type: 'addListItem',
                query: {
                    listId: req.params.listId,
                    groupId: req.params.groupId,
                    fullTitle: req.body.fullTitle,
                    userId: cachedTokenEntry.userId,
                }
            }, (err, response) => {
                if (err) {
                    console.error(err);
                    return res.status(500).json({ err }).end();
                }
                if (response.errCode) {
                    res.status(response.errCode).json({ errKey: response.errKey }).end();
                    return;
                }
                res.json({ data: response.data });
                return;
            });
        } catch(err) {
            console.error(err);
            res.status(500).send({ err });
        }
    });

    router.post('/api/lists/:listId/groups', async (req, res) => {
        try {
            const cachedTokenEntry = getCachedItemByKey('authTokens', req.cookies && req.cookies.authToken);
            if (!cachedTokenEntry) {
                return res.status(401).end();
            }
            client.send({
                type: 'addListGroup',
                query: {
                    listId: req.params.listId,
                    groupTitle: req.body.groupTitle,
                    userId: cachedTokenEntry.userId,
                }
            }, (err, response) => {
                if (err) {
                    console.error(err);
                    return res.status(500).json({ err }).end();
                }
                if (response.errCode) {
                    res.status(response.errCode).json({ errKey: response.errKey }).end();
                    return;
                }
                res.json({ data: response.data });
                return;
            });
        } catch(err) {
            console.error(err);
            res.status(500).send({ err });
        }
    });

    return router;
};
