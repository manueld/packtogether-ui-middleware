const express = require('express');
const router = express.Router();

module.exports = ({ listRequester, authRequester }) => {
    router.use(require('./auth')(authRequester));
    router.use(require('./lists')(listRequester));

    return router;
};
