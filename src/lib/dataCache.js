const R = require('ramda');

const _dataCache = {
    authTokens: {}
};

const _validateSection = section => {
    if (!section in _dataCache) {
        throw new Error(`Section "${section}" is not valid.`);
    }
}

const putToCache = (section, key, value) => {
    _validateSection(section);
    _dataCache[section][key] = value;
    console.log('CACHE', _dataCache);
    return true;
};

const getCachedItemByKey = (section, key) => {
    _validateSection(section);
    return _dataCache[section][key] || null;
};

const removeFromCache = (section, key) => {
    _validateSection(section);
    if (Object.keys(_dataCache[section]).includes(key)) {
        delete _dataCache[section][key];
        return true;
    }
    console.log('CACHE', _dataCache);
    return false;
};

module.exports = {
    putToCache,
    removeFromCache,
    getCachedItemByKey
};
