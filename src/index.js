const cote = require('cote');
const express = require('express');
const bodyParser = require('body-parser');
const cookieParser = require('cookie-parser');
const app = express();
const server = require('http').Server(app);

const listRequester = new cote.Requester({
    name: 'List Requester',
    key: 'list' // kind of domain
});
const authRequester = new cote.Requester({
    name: 'Auth Requester',
    key: 'user' // kind of domain
});

const routes = require('./lib/routes/')({ listRequester, authRequester });

app.use(bodyParser.json({ strict: true }));
app.use(cookieParser());
app.use(routes);
app.get('/health', (req, res, next) => {
    res.send('OK');
    console.log('HealthCheck: OK');
    return;
});
app.use((err, req, res, next) => {
    // default error handler
    console.error(err.stack);
    res.status(500).json(err);
});

server.listen(5599);
